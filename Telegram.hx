package telegramAPI;
import haxe.Utf8;
import haxe.ds.Option;
import neko.Lib;
import haxe.Http;
import haxe.Int64;
import haxe.Json;
import sys.db.Types.SBigId;
/**
 * ...
 * @author lightX2
 */

 
typedef ReplyKeyboardMarkup = {
  keyboard: Array<Keyboard>,               
  resize_keyboard: Bool,           
  one_time_keyboard: Bool,         
  selective: Bool                  
}

typedef ReplyKeyboardHide = {
  hide_keyboard: Bool,              
  selective: Bool                  
}
typedef Keyboard = {
  text: String                               
} 

typedef JSTUser = {
	id : String,
	?username : String,
	?first_name : String,
	?last_name : String,
	?language_code : String
}

typedef JSTContact = {
	is_bot : Bool,
	?phone_number : 	String,
	first_name 	:String,
	?last_name 	:String,
	?user_id 	:String,
	?language_code  : String,
	?id 	:String	
}

typedef JSTMessage = {
	chat : {
		type : String,
		id : String,
		?username : String,
		?first_name : String,
		?last_name : String,
	},
	? reply_to_message:JSTMessage,
	? date : String,
	from : JSTUser,
	text : String,
	message_id : Int,
	?contact : JSTContact
}

typedef JSTInlineQuery = {
	var id : String ;
	var from :JSTUser;
	@:optional var location : { 
		var longitude : Float ;
		var latitude : Float;
	};
	var query :	String;
	var offset :	String;
}

typedef JSTCallbackQuery= {
	var id : String ;
	var from :JSTUser;
	@:optional var message: JSTMessage;
	@:optional var inline_message_id :	String;
	@:optional var data:	String;
}

typedef JSTObject = {
	update_id:Int,
	?message : JSTMessage,
	?edited_message : JSTMessage,
	?inline_query : JSTInlineQuery,
	?callback_query : JSTCallbackQuery,
		
}

typedef JSTUpdates = {
	var ok:Bool;
	var result:Array<JSTObject>;
}

typedef TelegramAPIRequestParameter = {
	? url:String
	//? data:Map<String,String>;	
	
}

enum RequestStatus {
	Ok;
	ErrorConnection;
	ErrorNotJson;
}

class Telegram
{
	var APIUrl : String;
	public var verbose:Int;
	private var _token : String;
	public var updateOffset : Int;
	public var parameters:Map<String,String>;
	public var jsanswer :JSTUpdates;
	public var filter:String;
	
	private var proxyIP       : String;
	private var proxyPort     : Int;
	private var proxyUsername : String;
	private var proxyPass     : String;
	private var existProxy    : Bool;
	public function new(token:String) {
		//super();
		parameters = new Map<String,String>();
		setToken(token);
		updateOffset = -5000;
		filter = ""; 
		verbose = 0;
		existProxy = false;
		APIUrl = "https://api.telegram.org/bot";
	}
	
	public function setToken(token:String) {
		_token = token;
	}
	
	public function getAPIURL() :String {
		return APIUrl + _token +"/";
	}
	
	public function setProxy(ip:String, port:Int,
							username:String, pass:String) {
			existProxy     = ip.length > 0;
			proxyIP        = ip;
			proxyPort      = port;
			proxyUsername  = username;
			proxyPass      = pass;
	}
	
	public function setApiURL(url:String) {
		APIUrl = url+"/bot";
	}
	public function setOffset(offset:Int) {
		updateOffset = offset;
	}
	
	public function getNextUpdates():RequestStatus {
		var answer:String = "";
		try {
			//if  (existProxy){
				//Http.PROXY =  { host : proxyIP, port : proxyPort,
							//auth : { user : proxyUsername, pass : proxyPass} };
				//if (verbose > 1)
					//Lib.println("request with proxy");
			//}
			//else
				//Http.PROXY = null;
			//if (verbose > 1)
			answer = Http.requestUrl(getAPIURL()+ "getUpdates?offset=" + Std.string(updateOffset) +
										"&timeout=300");
		} catch (e:Dynamic) {
			if (verbose > 1) {
				Lib.println("request:"+getAPIURL()+ "getUpdates?offset=" + Std.string(updateOffset) +
									"&timeout=300");
				Lib.println("request api failed :" + Json.stringify(e));
			}
			return ErrorConnection;
		}
	    
		try {
			jsanswer = Json.parse(Utf8.decode(answer));
		} catch (e:Dynamic) {
			if (verbose > 1) {
				Lib.println("not json!");
				Lib.println(answer);
				
			}
			return ErrorNotJson;
		}
		return Ok;
	}
	
	
	/**
	 * requestApi with prepared parameters
	 * @param	url
	 */
	public function requestApiPrepared(url:String):String {
		var telegramRequest = new Http(getAPIURL() + url);
		var ret:String = "";
		telegramRequest.onData = function(msg:String) {
			ret = msg;
			Lib.println(msg);
		}
		telegramRequest.onError = function(msg:String) {
			Lib.println("error when send to url:"+url);
			Lib.println("error :" + msg);
			Lib.println("raw response:"+telegramRequest.responseData);
			Lib.println(Json.stringify(parameters));
		}
		for (pname in parameters.keys()) {
			telegramRequest.setParameter(pname,parameters.get(pname));
		}
		Lib.println("url : " + getAPIURL() + url);
		
		telegramRequest.request(true);
		return ret;
	}
	
	/**
	 * requestApi 
	 * @param	url
	 * @param	data
	 * @param	String>
	 */
	public function requestApi(url:String, data:Map<String,String>):String {
		var telegramRequest = new Http(getAPIURL() + url);
		var ret:String = "";
		telegramRequest.onData = function(msg:String) {
			ret = msg;
			Lib.println("  ANSWERED :"+msg);
		}
		telegramRequest.onError = function(msg:String) {
			Lib.println("error when send to url:"+url);
			Lib.println("error :"+msg);
			Lib.println("raw response:"+telegramRequest.responseData);
			Lib.println(Json.stringify(data));
		}
		for (pname in data.keys()) {
			telegramRequest.setParameter(pname,data.get(pname));
		}
		Lib.println("url : " + getAPIURL() + url);
		
		telegramRequest.request(true);
		return ret;
	}
	
	
	
	
	
	public function sendMessageWithRequest(chat_id:String, message:String,reply_id:String="") {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("text", message);
		parameters.set("parse_mode", "Markdown");
		parameters.set("disable_web_page_preview", "true");
		if (reply_id != "")
			parameters.set("reply_to_message_id", reply_id);
		parameters.set("reply_markup", '{"keyboard" : [[' +
						'{"text" :"Разрешить"},{"text":"Отказ"}]],' +
						'"resize_keyboard" : true,"one_time_keyboard" : true, ' +
						'"selective" : true}');

			
		requestApi("sendMessage", parameters);
	}
	
	
	
	public function editMessage(chat_id:String, message_id:String, message:String) {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("message_id", message_id);
		parameters.set("text", message);
		parameters.set("disable_web_page_preview", "true");
		//parameters.set("reply_markup", "{\"hide_keyboard\" : true}");
		requestApi("editMessageText", parameters);
	}
	
	public function editInlineMessage(inline_message_id:String, message:String) {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("inline_message_id", inline_message_id);
		parameters.set("text", message);
		parameters.set("disable_web_page_preview", "true");
		//parameters.set("reply_markup", "{\"hide_keyboard\" : true}");
		requestApi("editMessageText", parameters);
	}
	
	
	public function sendMessage(chat_id:String, message:String,reply_id:String="") {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("text", message);
		parameters.set("disable_web_page_preview", "true");
		if (reply_id != "")
			parameters.set("reply_to_message_id", reply_id);
		parameters.set("reply_markup", "{\"remove_keyboard\" : true}");
		//if (answerMsg)
			//parameters.set("text", message);
			
		requestApi("sendMessage", parameters);
	}
	
	public function answerInline(inline_id:String, message:String , switch_pm_text:String = "",
								switch_pm_parameter:String = "") {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("inline_query_id", inline_id);
		parameters.set("results", message);
		parameters.set("is_personal", "true");
		parameters.set("cache_time", "10");
		if (switch_pm_text.length>0 && switch_pm_parameter.length>0) {
			parameters.set("switch_pm_text", switch_pm_text);
			parameters.set("switch_pm_parameter", switch_pm_parameter);
		}
			
		requestApi("answerInlineQuery", parameters);
	}
	
	public function sendMessageNoHide(chat_id:String, message:String,reply_id:String="") {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("text", message);
		if (reply_id != "")
			parameters.set("reply_to_message_id", reply_id);
		//if (answerMsg)
			//parameters.set("text", message);
			
		requestApi("sendMessage", parameters);
	}
	
	public function sendMarkDownMessage(chat_id:String, message:String,reply_id:String="",hideKeyboard:Bool = false) {
		var parameters:Map<String,String>  = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("text", message);
		parameters.set("parse_mode", "Markdown");
		parameters.set("disable_web_page_preview", "true");
		if (reply_id != "")
			parameters.set("reply_to_message_id", reply_id);
		if (hideKeyboard)
			parameters.set("reply_markup", "{\"remove_keyboard\" : true}");

		var strList = message.split("\n");
		var currStr = "";
		var LIMIT_MESSAGE_LENGTH = 7500;
		
		for (i in 0...strList.length) {
			if (currStr.length + strList[i].length < LIMIT_MESSAGE_LENGTH) {
				currStr += strList[i] + "\n";
			} else {
				parameters.set("text", currStr);
				requestApi("sendMessage", parameters);
				currStr = "";
			}
		}
		if (currStr.length > 0) {
			parameters.set("text", currStr);
			requestApi("sendMessage", parameters);
		}
	}
	
	
	public function printTextWithMenu(chat_id:String, msg:String,menu:String):Bool {
		parameters = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("text", msg);
		parameters.set("reply_markup", menu);
		parameters.set("parse_mode", "Markdown");
		parameters.set("disable_web_page_preview", "true");
		var ret = requestApiPrepared("sendMessage");
		var jret:Dynamic;
		try {
			jret = Json.parse(ret);
		} catch (e:Dynamic){
			return false;
		}
		if (Std.string(jret.ok) == "true")
			return true;
		return false;
	}
	
	public function editMessageWithMenu(chat_id:String, message_id:String,
							msg:String,menu:String):Bool {
		parameters = new Map<String,String>();
		parameters.set("chat_id", chat_id);
		parameters.set("message_id", message_id);
		parameters.set("text", msg);
		parameters.set("reply_markup", menu);
		parameters.set("parse_mode", "Markdown");
		parameters.set("disable_web_page_preview", "true");
		var ret = requestApiPrepared("editMessageText");
		var jret:Dynamic;
		try {
			jret = Json.parse(ret);
		} catch (e:Dynamic){
			return false;
		}
		if (Std.string(jret.ok) == "true")
			return true;
		return false;
	}
	
	
}